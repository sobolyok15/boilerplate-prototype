'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = _interopDefault(require('react'));

function styleInject(css, ref) {
  if (ref === void 0) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') {
    return;
  }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css = ".auth_scss-test__1vpQL{margin-top:50px;color:#000;font-size:30px}";
var styles = {"scss-test":"auth_scss-test__1vpQL"};
styleInject(css);

var AuthContainer = function AuthContainer() {
  return React.createElement("div", {
    className: styles['scss-test']
  }, "I am auth container");
};

var GamesContainer = function GamesContainer() {
  return React.createElement("div", null, "I am games container");
};

var log = function log() {
  return console.log('log from core test');
};

exports.log = log;
exports.AuthContainer = AuthContainer;
exports.GamesContainer = GamesContainer;
