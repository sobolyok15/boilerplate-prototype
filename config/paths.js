const path = require('path');

module.exports = {
  entry: path.join(process.cwd(), 'boilerplate/src/index.js'),
  pathToHtml: path.join(process.cwd(), 'boilerplate/public/index.html'),
  contentBase: path.join(process.cwd(), 'dist'),
  resolve: {
    alias: {
      components: path.join(process.cwd(), 'boilerplate/src/components'),
      containers: path.join(process.cwd(), 'boilerplate/src/containers'),
      store: path.join(process.cwd(), 'boilerplate/src/store'),
      '@core': path.join(process.cwd(), 'core/src'),
    },
  },
};
